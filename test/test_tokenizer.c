#include "test.h"
#include "tokenizer.h"
#include "unity.h"

#include <string.h>

// === CONVENIENCE FUNCTIONS FOR THE TESTS =========================================== //

#define EXPECTATION_BUFFER 32

typedef const char *string_t;

static string_t expected_tokens[EXPECTATION_BUFFER];
static uint32_t expected_token_lengths[EXPECTATION_BUFFER];
static uint32_t n_expected_tokens;

/**
 * @brief Creates an expectation for a single token to be checked, using given().
 * @see given().
 */
static void expect_token(string_t token, uint32_t token_length) {
    expected_tokens[0] = token;
    expected_token_lengths[0] = token_length;
    n_expected_tokens = 1;
}

/**
 * @brief Creates an expectation for multiple tokens to be checked, using given().
 * @see given().
 */
static void expect_tokens(string_t *tokens, uint32_t *token_lengths, uint32_t n_tokens) {
    memcpy(expected_tokens, tokens, n_tokens * sizeof(string_t));
    memcpy(expected_token_lengths, token_lengths, n_tokens * sizeof(uint32_t));
    n_expected_tokens = n_tokens;
}

/**
 * @brief Creates an expectation that no more tokens should be present.
 * @see given().
 */
static void expect_none() {
    n_expected_tokens = 0;
}

/**
 * @brief Checks the next token from the specified tokenizer against the expected results.
 */
static void given(tokenizer_t tokenizer) {

    if (n_expected_tokens == 0) {
        TEST_ASSERT_FALSE_MESSAGE(tokenizer_has_next(tokenizer),
                                  "The tokenizer should not have any more tokens.");
        return;
    }

    for (int i = 0; i < n_expected_tokens; i++) {
        uint32_t token_length;

        TEST_ASSERT_TRUE_MESSAGE(tokenizer_has_next(tokenizer),
                                 "The tokenizer should find at least one token.");

        const char *token = tokenizer_get_next(tokenizer, &token_length);
        TEST_ASSERT_EQUAL_CHAR_ARRAY(expected_tokens[i], token,
                                     expected_token_lengths[i]);
        TEST_ASSERT_EQUAL_UINT32(expected_token_lengths[i], token_length);
    }
}

// === TESTS ========================================================================= //

static tokenizer_t tokenizer;

TEAR_DOWN {
    tokenizer_destroy(tokenizer);
}

TEST_SHOULD(create_tokenizer) {
    const char string[] = "Some Test String.";
    uint32_t length = sizeof(string) -
                      1; // The last character (\0) is also included when using sizeof.

    tokenizer = tokenizer_create(string, length, ' ');

    TEST_ASSERT_NOT_NULL_MESSAGE(tokenizer, "The tokenizer should not be null.");
}

TEST_SHOULD(return_zero_tokens) {
    const char string[] = "      ";
    uint32_t length = sizeof(string) - 1;

    tokenizer = tokenizer_create(string, length, ' ');

    expect_none();
    given(tokenizer);
}

TEST_SHOULD(return_one_token_from_string_without_delimiters) {
    const char string[] = "Some Test String.";
    uint32_t length = sizeof(string) - 1;

    tokenizer = tokenizer_create(string, length, ',');

    expect_token(string, length);
    given(tokenizer);

    expect_none();
    given(tokenizer);
}

TEST_SHOULD(return_one_token_from_string_with_delimiters) {
    const char string[] = "Some Other Test String.";
    uint32_t length = sizeof(string) - 1;

    const char token[] = "Some Other Test String";
    uint32_t token_length = sizeof(token) - 1;

    tokenizer = tokenizer_create(string, length, '.');

    expect_token(token, token_length);
    given(tokenizer);

    expect_none();
    given(tokenizer);
}

TEST_SHOULD(return_multiple_tokens) {
    const char string[] = "Some Other Test String.";
    uint32_t length = sizeof(string) - 1;

    string_t tokens[] = {"Some", "Other", "Test", "String."};
    uint32_t token_lengths[] = {4, 5, 4, 7};
    uint32_t n_tokens = sizeof(tokens) / sizeof(string_t);

    tokenizer = tokenizer_create(string, length, ' ');

    expect_tokens(tokens, token_lengths, n_tokens);
    given(tokenizer);

    expect_none();
    given(tokenizer);
}

TEST_SHOULD(skip_multiple_subsequent_delimiters) {
    const char string[] = "Lots of         spaces.";
    uint32_t length = sizeof(string) - 1;

    string_t tokens[] = {"Lots", "of", "spaces."};
    uint32_t token_lengths[] = {4, 2, 7};
    uint32_t n_tokens = sizeof(tokens) / sizeof(string_t);

    tokenizer = tokenizer_create(string, length, ' ');

    expect_tokens(tokens, token_lengths, n_tokens);
    given(tokenizer);

    expect_none();
    given(tokenizer);
}
