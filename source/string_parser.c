#include "string_parser.h"

#include "assertion.h"

#include <malloc.h>

uint32_t string_to_unsigned_integer(const char *string, uint32_t length) {
    ASSERT(string_is_unsigned_integer(string, length));

    uint32_t number = 0;
    for (uint32_t i = 0; i < length; i++) {
        number *= 10;
        number += (string[i] - '0');
    }

    return number;
}

uint8_t string_is_unsigned_integer(const char *string, uint32_t length) {
    if (string == NULL || length <= 0) {
        return 0;
    }

    for (uint32_t i = 0; i < length; i++) {
        if (string[i] < '0' || string[i] > '9') {
            return 0;
        }
    }
    return 1;
}
