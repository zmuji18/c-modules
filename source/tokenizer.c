#include "tokenizer.h"

#include "assertion.h"

#include <malloc.h>

// === STRUCT DEFINITIONS ============================================================ //

struct tokenizer_t {
    const char delimiter;
    const char *cursor;
    const char *end;
};

// === PRIVATE FUNCTIONS ============================================================= //

/**
 * @brief Advances the cursor of the tokenizer so that all contiguous delimiter characters
 * will be skipped until the next non-delimiter character.
 */
static void skip_delimiters(tokenizer_t tokenizer) {
    while (*tokenizer->cursor == tokenizer->delimiter &&
           tokenizer->cursor < tokenizer->end) {
        tokenizer->cursor++;
    }
}

// === IMPLEMENTATIONS =============================================================== //

tokenizer_t tokenizer_create(const char *string, uint32_t length, char delimiter) {
    ASSERT(string != NULL);
    ASSERT(length >= 0);

    tokenizer_t tokenizer = allocate(tokenizer_t);

    initialize_const(char, tokenizer->delimiter, delimiter);

    tokenizer->cursor = string;
    tokenizer->end = string + length;

    return tokenizer;
}

uint8_t tokenizer_has_next(tokenizer_t tokenizer) {
    ASSERT(tokenizer != NULL);

    skip_delimiters(tokenizer);
    return tokenizer->cursor < tokenizer->end;
}

const char *tokenizer_get_next(tokenizer_t tokenizer, uint32_t *token_length) {
    ASSERT(tokenizer != NULL);
    ASSERT(tokenizer_has_next(tokenizer));

    skip_delimiters(tokenizer);
    const char *token = tokenizer->cursor;

    while (*tokenizer->cursor != tokenizer->delimiter &&
           tokenizer->cursor < tokenizer->end) {
        tokenizer->cursor++;
    }

    if (token_length != NULL) {
        *token_length = tokenizer->cursor - token;
    }

    return token;
}

void tokenizer_destroy(tokenizer_t tokenizer) {
    ASSERT(tokenizer != NULL);
    free(tokenizer);
}
