#ifndef C_MODULES_CLASS_H
#define C_MODULES_CLASS_H

#include <malloc.h>

/**
 * @brief Creates a type definition which can be used as an object type, as if in an
 * object-oriented programming language. An immutable version of the type is also
 * generated.
 *
 * @param name The name of the type to be used.
 *
 * @note The name of the generated constant type is the name of the specified typed
 * prefixed with 'const_'.
 *
 * @example Specifying the type big_decimal_t would generate the following typedefs:<br>
 *          typedef struct big_decimal_t *big_decimal_t;                            <br>
 *          typedef const struct big_decimal_t *const_big_decimal_t;                <br>
 */
#define class(name)                                                                      \
    typedef struct name *name;                                                           \
    typedef const struct name *const_##name

/**
 * @brief Allocates space for the specified struct type.
 *
 * @param type The name of the struct type (excluding the 'struct' prefix).
 */
#define allocate(type) malloc(sizeof(struct type))

/**
 * @brief Initializes the specified variable of the given type (will silence compiler
 * warnings even if the variable is declared to be constant).
 *
 * @param type The type name excluding any qualifiers.
 * @param var The variable to be initialized.
 * @param value The value that the variable should be initialized with.
 */
#define initialize_const(type, var, value)                                               \
    do {                                                                                 \
        *(type *)(&(var)) = (value);                                                     \
    } while (0)

#endif // C_MODULES_CLASS_H
