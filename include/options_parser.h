#ifndef C_MODULES_OPTIONS_PARSER_H
#define C_MODULES_OPTIONS_PARSER_H

#include "class.h"

#include <inttypes.h>

class(options_parser_t);

/**
 * @brief Creates an options parser from the specified string.
 *
 * @param options The string containing the options.
 * @param options_length The length of the string containing the options.
 * @param option_marker The beginning character of a substring indicating a name of an
 * option.
 * @param delimiter The delimiter character used for separating tokens in the string.
 * @returns A reference to the options parser.
 */
options_parser_t options_parser_create(const char *options, uint32_t options_length,
                                       char option_marker, char delimiter);

/**
 * @brief Checks if the options parser has parsed an option with the specified name.
 *
 * @param options_parser A reference to the options parser struct.
 * @param option_name The name of the option without the options marker (i.e.
 * "-option_name" should be "option_name").
 * @returns A non-zero value if the option was parsed, zero otherwise.
 */
uint8_t options_parser_has_option(options_parser_t options_parser,
                                  const char *option_name);

/**
 * @brief Returns the token corresponding to the specified option.
 *
 * @param options_parser A reference to the options parser struct.
 * @param option_name The name of the option without the options marker (i.e.
 * "-option_name" should be "option_name").
 * @param value_length A reference to a variable which will be populated with the length
 * of the corresponding value.
 * @returns The corresponding option value.
 */
const char *options_parser_get_option(options_parser_t options_parser,
                                      const char *option_name, uint32_t *value_length);

/**
 * @brief Deallocates any resources used by the specified options parser.
 *
 * @param options_parser A reference to the options parser struct.
 * @returns void.
 */
void options_parser_destroy(options_parser_t options_parser);

#endif // C_MODULES_OPTIONS_PARSER_H
