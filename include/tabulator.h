#ifndef C_MODULES_TABULATOR_H
#define C_MODULES_TABULATOR_H

#include "class.h"
#include "string_wrapper.h"

#include <inttypes.h>

class(tabulator_t);

/**
 * @brief Creates a tabulator for the specified column array. The
 *
 * @param columns The array of string_t objects.
 * @param column_count The number of columns specified in the column array.
 * @returns A reference to the newly created tabulator.
 */
tabulator_t tabulator_create(string_t *columns, uint32_t column_count);

/**
 * @brief Adds the specified row values to the tabulator.
 *
 * @param tabulator A reference to the tabulator.
 * @param values The array of values in the row.
 * @param value_count The number of values in the row.
 * @returns void.
 */
void tabulator_add_row(tabulator_t tabulator, string_t *values, uint32_t value_count);

/**
 * @brief Clears all rows from this tabulator (except for the header).
 *
 * @param tabulator A reference to the tabulator.
 * @returns The number of rows cleared.
 */
uint32_t tabulator_clear(tabulator_t tabulator);

/**
 * @brief Deallocates any resources used by the specified tabulator.
 *
 * @param tabulator A reference to the tabulator.
 * @returns void.
 */
void tabulator_destroy(tabulator_t tabulator);

// === TABULATOR ROW ================================================================= //

class(tabulator_row_t);

/**
 * @brief Prints the contents of this row to the specified character buffer.
 *
 * @param row A reference to the tabulator row.
 * @param buffer The buffer to which the row content should be printed (assumed large
 * enough).
 * @param column_width The number of characters a single column should contain (left-over
 * padded with whitespace).
 * @param column_delimiter The delimiter character used to separate the printed columns.
 * @param row_delimiter The delimiter character used to separate the header row from the
 * rest.
 * @param cross_character The delimiter used whenever both a column and row delimiter
 * should be used.
 * @returns void.
 */
void tabulator_row_print(tabulator_row_t row, char *buffer, uint32_t column_width,
                         char column_delimiter, char row_delimiter, char cross_character);

// === TABULATOR ITERATOR ============================================================ //

class(tabulator_iterator_t);

/**
 * @brief Creates an iterator over the rows of the specified tabulator.
 *
 * @param tabulator A reference to the tabulator.
 * @returns A tabulator iterator that can be used to access the rows of this tabulator.
 */
tabulator_iterator_t tabulator_iterator_create(tabulator_t tabulator);

/**
 * @brief Checks if the iterator has any more elements left.
 *
 * @param iterator A reference to the iterator.
 * @returns A non-zero value if there are elements left to iterate over, zero otherwise.
 */
uint8_t tabulator_iterator_has_next(tabulator_iterator_t iterator);

/**
 * @brief Retrieves the next row from the iterator.
 *
 * @param iterator A reference to the iterator.
 * @returns A reference to the next row in the iterator.
 */
tabulator_row_t tabulator_iterator_get_next(tabulator_iterator_t iterator);

/**
 * @brief Deallocates any memory used by the specified iterator.
 *
 * @param iterator A reference to the iterator.
 * @returns void.
 */
void tabulator_iterator_destroy(tabulator_iterator_t iterator);

#endif // C_MODULES_TABULATOR_H
