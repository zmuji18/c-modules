#ifndef C_MODULES_ASSERTIONS_H
#define C_MODULES_ASSERTIONS_H

#include <assert.h>

#ifndef ASSERT
#define ASSERT(condition) assert(condition)
#endif

#endif // C_MODULES_ASSERTIONS_H
